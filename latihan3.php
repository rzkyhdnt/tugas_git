<?php
function tukar_besar_kecil($string){
//kode di sini
    $besar = range('A', 'Z');
    $kode = ['`', '-', ' '];
    $kecil = range('a', 'z');
    $angka = [1, 2, 3, 4, 5, 6, 7, 8];
    $panjang = strlen($string);
    $kalimat = "";
    
    foreach(str_split($string) as $str){
        for($i = 0; $i < count($kecil); $i++){
            if($str == $kecil[$i]){
                $kalimat .= strtoupper($kecil[$i]);
            } else if($str == $besar[$i]){
                $kalimat .= strtolower($besar[$i]);
            } 
        }
    }
    return $kalimat . "<br>";

}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>