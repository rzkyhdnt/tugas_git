<?php
function ubah_huruf($string){
//kode di sini
    $hasil = "";
    $abjad = range('a', 'z');
    $panjang = strlen($string);
    
    foreach(str_split($string) as $str){
        for($i = 0; $i < count($abjad); $i++){
            if(strtolower($str) == $abjad[$i]){
                $hasil .= $abjad[$i+1];
            }
        }
    }
    return $hasil . "<br>";

}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>